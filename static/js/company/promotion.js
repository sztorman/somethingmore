/**
 * Created by uziak on 07.05.14.
 */
window.getPromotionOffers = function (shop_id) {
    jQuery.ajax({
        type: 'POST',
        url: "/company/promotions/get/",
        dataType: "json",
        data: {
            'shop_id': shop_id,
            'csrfmiddlewaretoken': '{{ csrf_token }}'
        },
        error: function (data) {
            alert("Nie udało się pobrać promocji!");
        },
        success: function (response) {

            var promotions = response[0][1]
            var categories = response[0][3]

            for (i = 0; i < promotions.length; i++) {
                jQuery('#id-slider-promotion').append("<li style='display:none' promotion_id='" + promotions[i][0] + "'><div number='" + i + "' class='category_container'></div>" +
                    "<div class='span2010'><img id='promotion-image' src='/static/media/" + promotions[i][2] + "' width='200px' height='200px'/></div><div class='span2011'><div class='row2010 id-promotion-name'>" + promotions[i][1] + "</div><div class='row2010'><span class='promotion-price-before'>" + promotions[i][6] + "</span></div><div class='row2010'><span class='promotion-price-after'>" + promotions[i][7] + "</span></div><div class='promotion-time-period'>czas trwania promocji</div><div class='row2010'><span class='promotion-datepicker-from'>" + promotions[i][4] + "</span>-<span class='promotion-datepicker-to'>" + promotions[i][5] + "</span></div></div></li>")
                jQuery('.slider-permanent').find('li').eq(0).fadeIn(1000);

                var categoriesImages = "<BR>";
                var categoriesImages2 = "";
                for (iter = 0; iter < categories[i].length; iter++) {
                    if (categories[i][iter] != "") {
                        if (categories[i][iter] == "woman")  categoriesImages += "<img src='/static/img/coswiecej2_20.png' class='main-form-up' style='margin-left:3px' alt='My image' name='woman'/>"
                        if (categories[i][iter] == "man")  categoriesImages += "<img src='/static/img/coswiecej2_01_18.png' class='main-form-up' style='margin-left:3px' alt='My image' name='man'/>"
                        if (categories[i][iter] == "kid")  categoriesImages += "<img src='/static/img/coswiecej2_22.png' class='main-form-up' style='margin-left:3px' alt='My image' name='kid'/>"
                        if (categories[i][iter] == "summer")  categoriesImages += "<img src='/static/img/coswiecej2_24.png' class='main-form-up' style='margin-left:3px' alt='My image' name='summer'/>"
                        }
                    }

                 for (j = 0; j < mainFormIcons.length; j++) {
                     if (promotions[i][3] != "") {
                            if (mainFormIcons[j][4] == promotions[i][3]) {
                                categoriesImages += "<img src='/static/img/button_up/" + mainFormIcons[j][1] + "' class='main-form-up' style='margin-left:3px' alt='My image' name='" + mainFormIcons[j][4] + "'/>"
                                //jQuery("#promotion-categories").append(categoriesImages);
                            }
                     }
                 }
                //tu kopię butów trzeba dodać, nie same buty
                 jQuery(".category_container[number='"+i+"']").append(categoriesImages);
            }

            jQuery('#id-slider-promotion ul').first().addClass('active');
            jQuery('.popUp#user').dialog('destroy').remove()

        }
    });
    event.preventDefault();
}