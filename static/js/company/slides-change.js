/**
 * Created by uziak on 07.05.14.
 */
// settings
var $slider = jQuery('.slider-promotion'); // class or id of carousel slider
var $slide = 'li'; // could also use 'img' if you're not using a ul
var $transition_time = 1000; // 1 second

function slides() {
    return $slider.find($slide);
}

slides().fadeOut();

slides().first().addClass('active');
slides().first().fadeIn($transition_time);

jQuery('#next').click(function () {

    //get the right position
    var $i = $slider.find($slide + '.active').index();
    slides().eq($i).removeClass('active');
    //slide the item
    slides().eq($i).fadeOut($transition_time);

    if (slides().length == $i + 1) $i = -1; // loop to start

    slides().eq($i + 1).fadeIn($transition_time);
    slides().eq($i + 1).addClass('active');

    //getPromotionOffer(slides().eq($i + 1).attr('promotion_id'));
//stary mechanizm, który jest ok, ale trzeba wszystko przebudowac, zeby bylo dobrze
    /*       jQuery('li.active > .category_container img').each(

     function(){
     alert(jQuery(this).attr('name'));
     var cat = jQuery(this).attr('name');
     jQuery('#panel4 > .types').find("checkbox[value='" + cat + "']").prop('checked', true); // tutaj ma być kliknięte
     jQuery('#panel4 img.woman').click();
     }
     );*/

    //jQuery(".category_container img").last();
    //cancel the link behavior

    getPromotionDetails();

    return false;

});

//if mouse hover, pause the auto rotation, otherwise rotate it

// settings
var $sliderPermanent = jQuery('.slider-permanent'); // class or id of carousel slider
var $slide = 'li'; // could also use 'img' if you're not using a ul
var $transition_time = 1000; // 1 second


function slidesPermanent() {
    return $sliderPermanent.find($slide);
}

slidesPermanent().fadeOut();

slidesPermanent().first().addClass('active');
slidesPermanent().first().fadeIn($transition_time);

//if user clicked on next button
jQuery('#next-permanent').click(function () {

    //get the right position
    var $ik = $sliderPermanent.find($slide + '.active').index();
    slidesPermanent().eq($ik).removeClass('active');
    //slide the item
    slidesPermanent().eq($ik).fadeOut($transition_time);

    if (slidesPermanent().length == $ik + 1) $ik = -1; // loop to start

    slidesPermanent().eq($ik + 1).fadeIn($transition_time);
    slidesPermanent().eq($ik + 1).addClass('active');

    // getPermanentOffer(slidesPermanent().eq($ik + 1).attr('permanent_id'));

    getPermanentDetails();

    return false;

});

jQuery('#prev-permanent').click(function () {

    //get the right position
    var $ik = $sliderPermanent.find($slide + '.active').index();
    slidesPermanent().eq($ik).removeClass('active');
    //slide the item
    slidesPermanent().eq($ik).fadeOut($transition_time);

    if (slidesPermanent().length == $ik - 1) $ik = +1; // loop to start

    slidesPermanent().eq($ik - 1).fadeIn($transition_time);
    slidesPermanent().eq($ik - 1).addClass('active');

    //getPermanentOffer(slidesPermanent().eq($ik - 1).attr('permanent_id'));

    getPermanentDetails();

    return false;

});


getPermanentDetails = function () {

    var array = [];


    jQuery('li.active > .category_container_permanent img').each(

        function () {

            array.push(jQuery(this).attr('name'));

        }
    );

    var last = jQuery('li.active > .category_container_permanent img:last').attr('name');


    jQuery('li.active > .category_container_permanent').empty();
    jQuery.each(array, function (index, value) {

        jQuery("#panel2 img." + value).click();

    });
    jQuery('#ttipContent').find("img[name='" + last + "']").click();

    //  jQuery('a[href="panel2"]').trigger('click');

    jQuery('.show').removeClass('show');
    jQuery('#panel2').addClass('show');

    jQuery('#permanent-offer-price').val(jQuery('span.permanent-offer-price').text());
    jQuery('#id-permanent-name').val(jQuery('div.id-permanent-name').text());
    jQuery('#permanent-offer-price').val(jQuery('div.permanent-offer-price').text());
    jQuery('#permanent_id').val(jQuery('li.active').attr('permanent_id'));
}

getPromotionDetails = function () {
    var array = [];

    jQuery('li.active > .category_container img').each(

        function () {

            array.push(jQuery(this).attr('name'));

        }
    );

    var last = jQuery('li.active > .category_container img:last').attr('name');


    jQuery('li.active > .category_container').empty();
    jQuery.each(array, function (index, value) {

        jQuery("#panel4 img." + value).click();

    });
    jQuery('#ttipContent').find("img[name='" + last + "']").click();


    jQuery('.show').removeClass('show');
    jQuery('#panel4').addClass('show');

    jQuery('#promotion-price-before').val(jQuery('span.promotion-price-before').text());
    jQuery('#promotion-price-after').val(jQuery('span.promotion-price-after').text());
    jQuery('#id-promotion-date-from').val(jQuery('span.promotion-datepicker-from').text());
    jQuery('#id-promotion-date-to').val(jQuery('span.promotion-datepicker-to').text());
    jQuery('#promotion_id').val(jQuery('li.active').attr('promotion_id'));
    jQuery('#id-promotion-name').val(jQuery('div.id-promotion-name').text());
}