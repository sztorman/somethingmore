/**
 * Created by uziak on 07.05.14.
 */
 window.getShop = function(shop_id){
             jQuery.ajax({
                type: 'POST',
                url: "/company/shop/details/",
                dataType: "json",
                data: {
                    'shop_id': shop_id,
                    'csrfmiddlewaretoken': '{{ csrf_token }}'
                },
                error: function (result) {
                    alert("Nie udało się pobrać sklepu");
                },
                success: function (result) {
                    jQuery('#id-shop-name').val(result.name);
                    jQuery('.id-shop-name').html(result.name);
                    jQuery('#id-shop-description').val(result.description);
                    jQuery('.id-shop-description').html(result.description);
                    jQuery('#id-shop-www').val(result.page);
                    jQuery('.id-shop-www').html(result.page);
                    jQuery('#id-shop-city').val(result.city);
                    jQuery('.id-shop-city').html(result.city);
                    jQuery('#id-shop-address-street').val(result.street);
                    jQuery('.id-shop-address-street').html(result.street);
                    jQuery('#id-shop-phone').val(result.phone);
                    jQuery('.id-shop-phone').html(result.phone);
                    jQuery('#id-shop-email').val(result.email);
                    jQuery('.id-shop-email').val(result.email);
                    jQuery('#id-shop-address-street-number').val(result.street_number);
                    jQuery('.id-shop-address-street-number').val(result.street_number);
                    jQuery('#id-shop-address-street-flat').val(result.flat);
                    jQuery('.id-shop-address-street-flat').val(result.flat);
                    jQuery('#id_mon_start').val(result.mon_start);
                    jQuery('#id_mon_end').val(result.mon_end);
                    jQuery('#id_tue_start').val(result.tue_start);
                    jQuery('#id_tue_end').val(result.tue_end);
                    jQuery('#id_wed_start').val(result.wed_start);
                    jQuery('#id_wed_end').val(result.wed_end);
                    jQuery('#id_thur_start').val(result.thur_start);
                    jQuery('#id_thur_end').val(result.thur_end);
                    jQuery('#id_frid_start').val(result.fri_start);
                    jQuery('#id_frid_end').val(result.fri_end);
                    jQuery('#id_sat_start').val(result.sat_start);
                    jQuery('#id_sat_end').val(result.sat_end);
                    jQuery('#id_sun_start').val(result.sun_start);
                    jQuery('#id_sun_end').val(result.sun_end);
                    jQuery('span.hours[day="monday"]').html(result.mon_start + "-" + result.mon_end);
                    jQuery('span.hours[day="tuesday"]').html(result.tue_start + "-" + result.tue_end);
                    jQuery('span.hours[day="wednesday"]').html(result.wed_start + "-" + result.wed_end);
                    jQuery('span.hours[day="thursday"]').html(result.thur_start + "-" + result.thur_end);
                    jQuery('span.hours[day="friday"]').html(result.fri_start + "-" + result.fri_end);
                    jQuery('span.hours[day="saturday"]').html(result.sat_start + "-" + result.sat_end);
                    jQuery('span.hours[day="sunday"]').html(result.sun_start + "-" + result.sun_end);
                   // jQuery('.popUp#user').dialog('destroy').remove()
                }
            });
            event.preventDefault();
        }

window.getShops = function(){
             jQuery.ajax({
                type: 'POST',
                url: "/company/shops/list/",
                dataType: "json",
                 data: {
                    'csrfmiddlewaretoken': '{{ csrf_token }}'
                },
                error: function (result) {
                    alert("Nie udało się pobrać sklepu");

                },
                success: function (result){
              jQuery('#shop_list').empty();
              jQuery('#shop_list').append('<option selected="selected" value="-1">Nowy sklep</option>');
                    for (i=0;i<result.success.length;i++){
                        jQuery('#shop_list').append('<option selected="selected" value="'+ result.success[i][0] +'">'+ result.success[i][1] +'</option>');
                    }
                }
            });
            event.preventDefault();
        }