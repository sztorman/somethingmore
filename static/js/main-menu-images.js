var createImages = function(chooseArray){

mainFormIcons = []
mainFormIconsMan = []
mainFormIconsMan = [
    [1, "OKULARY_MS.png", "OKULARY.png", "OKULARY_M.png", "Okulary"],
    [2, "SWETRY.png", "SWETRY.png", "SWETRY_M.png", "Swetry"],
    [3, "T-SHIRT_KS.png", "T-SHIRTY.png", "T-SHIRT_M.png", "T-shirty"],
    [4, "0112.png", "0112.png", "0112.png", "Blank"],
    [5, "BUTY.png", "BUTY.png", "BUTY2_M.png", "Buty"],
    [6, "XXXL_S.png", "XXXL.png", "XXXL_M.png", "XXXL"],
    [7, "DODATKI.png", "DODATKI.png", "DODATKI_M.png", "Dodatki"],
    [8, "0112.png", "0112.png", "0112.png", "Blank"],
    [9, "BLUZKI_MS.png", "BLUZKI.png", "BLUZKI_M.png", "Bluzki"],
    [10, "SPORT.png", "SPORT.png", "SPORT_M.png", "Sportowe"],
    [11, "SWETRY.png", "SWETRY.png", "SWETRY_M.png", "Sweterki"],
    [12, "0112.png", "0112.png", "0112.png", "Blank"],
    [13, "BIELIZNA_MS.png", "BIELIZNA.png", "BIELIZNA_M.png", "Bielizna"],
    [14, "PLASZCZ_MS.png", "PLASZCZE.png", "PLASZCZ_M.png", "Płaszcze"],
    [15, "SPODNIE.png", "SPODNIE.png", "SPODNIE_M.png", "Spodnie"],
    [16, "BLUZY.png", "BLUZY.png", "BLUZY_M.png", "Bluzy"],
    [17, "0112.png", "0112.png", "0112.png", "Blank"],
    [18, "NESESERY.png", "NESESERY.png", "NESESERY_M.png", "Nesesery"],
    [19, "KRAWATY_MS.png", "KRAWATY.png", "KRAWATY_M.png", "Krawaty"],
    [20, "0112.png", "0112.png", "0112.png", "Blank"]
]


mainFormIconsWoman = []
mainFormIconsWoman = [
    [1, "BIELIZNA_KS.png", "BIELIZNA.png", "BIELIZNA_K.png", "Bielizna"],
    [2, "BLUZKI_KS.png", "BLUZKI.png", "BLUZKI_K.png", "Bluzki"],
    [3, "BUTY3_KS.png", "BUTY.png", "BUTY2_K.png", "Buty"],
    [4, "0112.png", "0112.png", "0112.png", "Blank"],
    [5, "CIAZA_KS.png", "CIAZOWE.png", "CIAZA.png", "Ciąża"],
    [6, "0112.png", "0112.png", "0112.png", "Blank"],
    [7, "OKULARY_KS.png", "OKULARY.png", "OKULARY_K.png", "Okulary"],
    [8, "SPODNICE_S.png", "SPODNICE.png", "SPODNICE.png", "Spódnice"],
    [9, "PLASZCZ_KS.png", "PLASZCZE.png", "PLASZCZ_K.png", "Płaszcz"],
    [10, "0112.png", "0112.png", "0112.png", "Blank"],
    [11, "T-SHIRT_KS.png", "T-SHIRTY.png", "T-SHIRT_K.png", "T-shirt"],
    [12, "0112.png", "0112.png", "0112.png", "Blank"],
    [13, "ZAKIETY_KS.png", "ZAKIETY.png", "ZAKIETY_K.png", "Żakiet"],
    [14, "0112.png", "0112.png", "0112.png", "Blank"],
    [15, "SPODNIE.png", "SPODNIE.png", "SPODNIE_K.png", "Spodnie"],
    [16, "BLUZY.png", "BLUZY.png", "BLUZY_K.png", "Bluzy"],
    [17, "0112.png", "0112.png", "0112.png", "Blank"],
    [18, "DODATKI.png", "DODATKI.png", "DODATKI_K.png", "Dodatki"],
    [19, "0112.png", "0112.png", "0112.png", "Blank"    ],
    [20, "SWETRY.png", "SWETRY.png", "SWETRY_K.png", "Swetry"]
]

mainFormIconsChild = []
mainFormIconsChild = [
    [1, "ZAKIETY_KS.png", "ZAKIETY.png", "ZAKIETY_K.png", "Żakiety"],
    [2, "0112.png", "0112.png", "0112.png", "Blank"],
    [3, "T-SHIRT_MS.png", "T-SHIRTY.png", "T-SHIRT_M.png", "T-shirt"],
    [4, "SWETRY.png", "SWTRY.png", "SWETRY_K.png", "Swetry"]
    [5, "SUKIENKI.png", "SUKIENKI.png", "SUKIENKI_K.png", "Sukienki"],
    [6, "BLUZY.png", "0112.png", "0112.png", "Blank"],
    [7, "SPORT.png", "SPORT.png", "SPORT_K.png", "Sport"],
    [8, "SPODNIE.png", "SPODNIE.png", "SPODNIE_K.png", "Spodnie"],
    [9, "SPODNICE_S.png", "SPODNICE.png", "SPODNICE.png", "Spódnice"],
    [10, "BLUZKI_MS.png", "BLUZKI.png", "BLUZKI_M.png", "Bluzki"],
    [11, "OKULARY_MS.png", "OKULARY.png", "OKULARY_M.png", "T-shirt"],
    [12, "0112.png", "0112.png", "0112.png", "Blank"],
    [13, "kurtki-PLASZCZ_MS.png", "PLASZCZE.png", "Kurtki-PLASZCZ_M.png", "Płaszcze"],
    [14, "BIELIZNA_MS.png", "Bielizna.png", "Bielizna_M.png", "Bielizna"],
    [15, "KOSZULE_MS.png", "KOSZULE.png", "KOSZULE_M.png", "Koszule"],
    [16, "GARNITURY.png", "GARNITURY.png", "GANITURY_M.png", "Garnitury"],
    [17, "0112.png", "0112.png", "0112.png", "Blank"],
    [18, "DODATKI.png", "DODATKI.png", "DODATKI_M.png", "Dodatki"],
    [19, "0112.png", "0112.png", "0112.png", "Blank"    ],
    [20, "BUTY.png", "BUTY.png", "BUTY_K.png", "Buty"]
]

mainFormWideIcons = []
mainFormWideIcons = [
    [1, "TOREBKI_KS.png", "TOREBKI.png", "TOREBKI_K.png", "Torebki"],
    [2, "STROJ_PL_KS.png", "STROJ_PLAZOWY.png", "STROJ_PL_K.png", "Strój plażowy"],
    [3, "KOSZULE_KS.png", "KOSZULE.png", "KOSZULE_K.png", "Koszule"],
    [4, "021.png", "021.png", "021.png", "Blank"],
    [5, "021.png", "021.png", "021.png", "Blank"],
    [6, "TOPY_KS.png", "TOPY.png", "TOPY_K.png", "Topy"],
    [7, "CZ_SZ_RE_KS.png", "CZ_SZ_RE.png", "CZ_SZ_RE_K.png", "Rękawiczki, szaliki, czapki"],
    [8, "CZ_SZ_RE_KS.png", "CZ_SZ_RE.png", "CZ_SZ_RE_K.png", "Rękawiczki, szaliki, czapki"]
]

mainFormWideIconsMan = []
mainFormWideIconsMan = [
    [1, "TOREBKI_MS.png", "TOREBKI.png", "TOREBKI_M.png", "Torebki"],
    [2, "TOPY_KS.png", "TOPY.png", "TOPY_M.png", "Topy"],
    [3, "STROJ_PL_MS.png", "STROJ_PLAZOWY.png", "STROJ_PL_M.png", "Stroje plażowe"],
    [4, "KOSZULE_MS.png", "KOSZULE.png", "KOSZULE_M.png", "Koszule"],
    [5, "GARNITURY.png", "GARNITURY.png", "GARNITURY_M.png", "Garnitury"],
    [6, "021.png", "021.png", "024.png", "Blank"],
    [7, "CZ_SZ_RE_MS.png", "CZ_SZ_RE.png", "CZ_SZ_RE_M.png", "Rękawiczki, szaliki, czapki"],
    [8, "021.png", "021.png", "024.png", "Blank"]
]

mainFormWideIconsChild = [];
mainFormWideIconsChild = [
    [1, "TOREBKI_MS.png", "TOREBKI.png", "TOREBKI_M.png", "Torebki"],
    [2, "TOPY_MS.png", "TOPY.png", "TOPY_M.png", "Topy"],
    [3, "STROJ_PL_MS.png", "STROJ_PLAZOWY.png", "STROJ_PL_M.png", "Stroje plażowe"],
    [4, "KOSZULE_MS.png", "KOSZULE.png", "KOSZULE_M.png", "Koszule"],
    [5, "GARNITURY.png", "GARNITURY.png", "GARNITURY_M.png", "Garnitury"],
    [6, "021.png", "021.png", "024.png", "Blank"],
    [7, "CZ_SZ_RE_MS.png", "CZ_SZ_RE.png", "CZ_SZ_RE_M.png", "Rękawiczki, szaliki, czapki"],
    [8, "021.png", "021.png", "024.png", "Blank"]
]

mainFormIconsBlank = [
    [1, "blank.png", "blank.png", "blank.png", "Blank"]
]

columnsTable = [];
columnsTable = ["span17", "span181", "span18", "span19"];

    if (chooseArray == "W") mainFormIcons = mainFormIconsWoman.slice();
    else if (chooseArray == "M") {
        mainFormWideIcons = mainFormWideIconsMan.slice();
        mainFormIcons = mainFormIconsMan.slice();
    }
    else{
        mainFormWideIcons = mainFormWideIconsMan.slice();
        mainFormIcons = mainFormIconsMan.slice();
    }


var u, x, o = 3;
while (o) {
    u = parseInt(Math.random() * o);
    x = columnsTable[--o];
    columnsTable[o] = columnsTable[u];
    columnsTable[u] = x;
}

var m, x, k = mainFormIcons.length;
while (k) {
    m = parseInt(Math.random() * k);
    x = mainFormIcons[--k];
    mainFormIcons[k] = mainFormIcons[m];
    mainFormIcons[m] = x;
}

var mw, xw, kw = mainFormWideIcons.length;
while (kw) {
    mw = parseInt(Math.random() * kw);
    xw = mainFormWideIcons[--kw];
    mainFormWideIcons[kw] = mainFormWideIcons[mw];
    mainFormWideIcons[mw] = xw;
}

var c = 0, html = "";
html = "<div class='span777'></div>";
while (c < 4) {
    html += '<div class=' + columnsTable[c] + '></div>';
    c++;
}
jQuery("div.row36").html(html);

var i = 0, j = 0;
jQuery('.span17').html('<label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i][4]+'" name="radios" value="'+ mainFormIcons[i][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i][1] + '" class="main-form-button-big main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[j][4]+'" name="radios" value="'+ mainFormIcons[j][4]+'" class="search"><img src="/static/img/button_up/' + mainFormWideIcons[j][1] + '" class="main-form-button-wide" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+1][4]+'" name="radios" value="'+ mainFormIcons[i+1][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 1][1] + '" class="main-form main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+2][4]+'" name="radios" value="'+ mainFormIcons[i+2][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 2][1] + '" class="main-form main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormWideIcons[j+1][4]+'" name="radios" value="'+ mainFormWideIcons[j+1][4]+'" class="search"><img src="/static/img/button_up/' + mainFormWideIcons[j + 1][1] + '""  class="main-form-button-wide main-navigation" alt="My image"/></label>');
jQuery('.span181').html('<label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+3][4]+'" name="radios" value="'+ mainFormIcons[i+3][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 3][1] + '" class="main-form main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+4][4]+'" name="radios" value="'+ mainFormIcons[i+4][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 4][1] + '" class="main-form main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+5][4]+'" name="radios" value="'+ mainFormIcons[i+5][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 5][1] + '" class="main-form main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+6][4]+'" name="radios" value="'+ mainFormIcons[i+6][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 6][1] + '" class="main-form main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormWideIcons[j+2][4]+'" name="radios" value="'+ mainFormWideIcons[j+2][4]+'" class="search"><img src="/static/img/button_up/' + mainFormWideIcons[j + 2][1] + '" class="main-form-button-wide" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+19][4]+'" name="radios" value="'+ mainFormIcons[i+19][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 19][1] + '" class="main-form-button-big main-navigation" alt="My image"/></label><div id="small-up"><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+8][4]+'" name="radios" value="'+ mainFormIcons[i+8][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 8][1] + '" class="main-form-small-up main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+9][4]+'" name="radios" value="'+ mainFormIcons[i+9][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 9][1] + '" class="main-form-small-up-second main-navigation" alt="My image"/></label></div><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+10][4]+'" name="radios" value="'+ mainFormIcons[i+10][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 10][1] + '" class="main-form main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+11][4]+'" name="radios" value="'+ mainFormIcons[i+11][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 11][1] + '" class="main-form main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+12][4]+'" name="radios" value="'+ mainFormIcons[i+12][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 12][1] + '" class="main-form main-navigation" alt="My image"/></label>');
jQuery('.span19').html('<label class="main-menu-choose"><input type="checkbox" id="'+ mainFormWideIcons[j+3][4]+'" name="radios" value="'+ mainFormIcons[j+3][4]+'" class="search"><img src="/static/img/button_up/' + mainFormWideIcons[j + 3][1] + '" class="main-form-button-wide" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+19][4]+'" name="radios" value="'+ mainFormIcons[i+19][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 19][1] + '" class="main-form-button-big main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+14][4]+'" name="radios" value="'+ mainFormIcons[i+14][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 14][1] + '" class="main-form main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+15][4]+'" name="radios" value="'+ mainFormIcons[i+15][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 15][1] + '" class="main-form main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormWideIcons[j+4][4]+'" name="radios" value="'+ mainFormIcons[j+4][4]+'" class="search"><img src="/static/img/button_up/' + mainFormWideIcons[j + 4][1] + '" class="main-form-button-wide" alt="My image"/></label>');
jQuery('.span18').html('<label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+16][4]+'" name="radios" value="'+ mainFormIcons[i+16][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 16][1] + '" class="main-form-button-big main-navigation" alt="My image"/></label><div id="small-up"><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+17][4]+'" name="radios" value="'+ mainFormIcons[i+17][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 17][1] + '" class="main-form-small-up main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+18][4]+'" name="radios" value="'+ mainFormIcons[i+18][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 18][1] + '" class="main-form-small-up-second main-navigation" alt="My image"/></label></div><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+18][4]+'" name="radios" value="'+ mainFormIcons[i+18][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 18][1] + '" class="main-form-button-big main-navigation" alt="My image"/></label><div id="small-up"><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+18][4]+'" name="radios" value="'+ mainFormIcons[i+18][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 18][1] + '" class="main-form-small-up main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+13][4]+'" name="radios" value="'+ mainFormIcons[i+13][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 13][1] + '" class="main-form-small-up-second main-navigation" alt="My image"/></label></div><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormIcons[i+7][4]+'" name="radios" value="'+ mainFormIcons[i+7][4]+'" class="search"><img src="/static/img/button_up/' + mainFormIcons[i + 7][1] + '" class="main-form main-navigation" alt="My image"/></label><label class="main-menu-choose"><input type="checkbox" id="'+ mainFormWideIcons[j+5][4]+'" name="radios" value="'+ mainFormWideIcons[j+5][4]+'" class="search"><img src="/static/img/button_up/' + mainFormWideIcons[j + 5][1] + '" class="main-form-button-wide" alt="My image"/></label>');
//}

jQuery(".main-navigation").mouseover(function () {
    var pictureName = jQuery(this).attr("src");
    pictureName = pictureName.replace('/static/img/button_up/', '', '');
    var t = 0, zmienna = -1;
    jQuery.each(mainFormIcons, function () {
        if (mainFormIcons[t][1] == pictureName) {
            zmienna = t;
        }
        t++;
    });

    var newSrc = "/static/img/button_over/" + mainFormIcons[zmienna][2];
   // alert(newSrc);
    jQuery(this).attr("src", newSrc);


});

jQuery(".main-navigation").mouseout(function () {
    var pictureName = jQuery(this).attr("src");
    pictureName = pictureName.replace('/static/img/button_over/', '', '');
    var t = 0, zmienna2 = -1;

    jQuery.each(mainFormIcons, function () {
        if (mainFormIcons[t][2] == pictureName) {
            zmienna2 = t;
        }
        t++;
    });

    var newSrc = "/static/img/button_up/" + mainFormIcons[zmienna2][1];
    jQuery(this).attr("src", newSrc);

});

jQuery(".main-navigation").click(function () {
    if (jQuery(this).hasClass('display-none')) {
        var pictureName = jQuery(this).attr("src");
        pictureName = pictureName.replace('/static/img/button_active/', '', '');
        var t = 0, zmienna3 = 3;

        jQuery.each(mainFormIcons, function () {
            if (mainFormIcons[t][3] == pictureName) {
                zmienna3 = t;
            }
            t++;
        });

        var newSrc = "/static/img/button_over/" + mainFormIcons[zmienna3][2];
        jQuery(this).attr("src", newSrc);
        jQuery(this).removeClass('display-none');
    } else {

          var pictureName = jQuery(this).attr("src");
        pictureName = pictureName.replace('/static/img/button_over/', '', '');
        var t = 0, zmienna3 = 3;

        jQuery.each(mainFormIcons, function () {
            if (mainFormIcons[t][2] == pictureName) {
                zmienna3 = t;
            }
            t++;
        });

        var newSrc = "/static/img/button_active/" + mainFormIcons[zmienna3][3];
        jQuery(this).attr("src", newSrc);
        jQuery(this).addClass('display-none');
    }
});

//dla szerokich

jQuery(".main-form-button-wide").mouseover(function () {
    var pictureName = jQuery(this).attr("src");
    pictureName = pictureName.replace('/static/img/button_up/', '', '');
    var t = 0, zmienna = -1;
    jQuery.each(mainFormWideIcons, function () {
        if (mainFormWideIcons[t][1] == pictureName) {
            zmienna = t;
        }
        t++;
    });

    var newSrc = "/static/img/button_over/" + mainFormWideIcons[zmienna][2];
   // alert(newSrc);
    jQuery(this).attr("src", newSrc);


});

jQuery(".main-form-button-wide").mouseout(function () {
    var pictureName = jQuery(this).attr("src");
    pictureName = pictureName.replace('/static/img/button_over/', '', '');
    var t = 0, zmienna2 = -1;

    jQuery.each(mainFormWideIcons, function () {
        if (mainFormWideIcons[t][2] == pictureName) {
            zmienna2 = t;
        }
        t++;
    });

    var newSrc = "/static/img/button_up/" + mainFormWideIcons[zmienna2][1];
    jQuery(this).attr("src", newSrc);

});

jQuery(".main-form-button-wide").click(function () {
    if (jQuery(this).hasClass('display-none')) {
        var pictureName = jQuery(this).attr("src");
        pictureName = pictureName.replace('/static/img/button_active/', '', '');
        var t = 0, zmienna3 = 3;

        jQuery.each(mainFormWideIcons, function () {
            if (mainFormWideIcons[t][3] == pictureName) {
                zmienna3 = t;
            }
            t++;
        });

        var newSrc = "/static/img/button_over/" + mainFormWideIcons[zmienna3][2];
        jQuery(this).attr("src", newSrc);
        jQuery(this).removeClass('display-none');
    } else {

          var pictureName = jQuery(this).attr("src");
        pictureName = pictureName.replace('/static/img/button_over/', '', '');
        var t = 0, zmienna3 = 3;

        jQuery.each(mainFormWideIcons, function () {
            if (mainFormWideIcons[t][2] == pictureName) {
                zmienna3 = t;
            }
            t++;
        });

        var newSrc = "/static/img/button_active/" + mainFormWideIcons[zmienna3][3];
        jQuery(this).attr("src", newSrc);
        jQuery(this).addClass('display-none');
    }
});

}