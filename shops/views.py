from urllib2 import URLError
from django.contrib.auth.decorators import user_passes_test, login_required

from django.contrib.gis import geos
from django.contrib.gis import measure
import json
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from geopy.geocoders.googlev3 import GoogleV3
from geopy.geocoders.googlev3 import GeocoderQueryError
from django.core.serializers.json import DjangoJSONEncoder

from shops import forms
from shops import models
from accounts.models import CompanyProfile
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from shops.models import Categories, MainCategories


def company_check(user):
    return user.groups.filter(name__in=['company'])


def geocode_address(address):
    address = address.encode('utf-8')
    geocoder = GoogleV3()
    try:
        _, latlon = geocoder.geocode(address)
    except (URLError, GeocoderQueryError, ValueError):
        return None
    else:
        return latlon


def get_shops(longitude, latitude):
    current_point = geos.fromstr("POINT(%s %s)" % (longitude, latitude))
    distance_from_point = {'km': 4}
    shops = models.Shop.gis.filter(location__distance_lte=(current_point, measure.D(**distance_from_point)))
    shops = shops.distance(current_point).order_by('distance')
    return shops.distance(current_point)


def home(request):
    form = forms.AddressForm()
    shops = []
    latitude = "52.2305687"
    longitude = "21.0156329"
    response_data = []
    if request.POST:
        form = forms.AddressForm(request.POST)
        if form.is_valid():
            address = form.cleaned_data['city'] + ", " + form.cleaned_data['address']
            location = geocode_address(address)
            types = request.POST.getlist('menu_types')
            #types.append(request.POST.getlist('radios'))
            if location:
                latitude, longitude = location
                #response_data[0][0] = longitude
                #response_data[0][1] = latitude
                shops = get_shops(longitude, latitude)
                shopsArray = [["" for i in range(6)] for j in range(len(shops))]
                categories_array = [["" for j in range(26)] for i in range(len(shops))]
                o = 0;
                for shop in shops:
                    categories = get_categories(shop.id)
                    dupa = list(set(types).intersection(set(categories)))
                    if len(set.intersection(set(types), set(categories))) or not types:
                        shopsArray[o][0] = shop.name
                        shopsArray[o][1] = shop.location.x
                        shopsArray[o][2] = shop.location.y
                        shopsArray[o][3] = str(shop.distance)
                        shopsArray[o][4] = shop.street + " " + shop.street_number
                        shopsArray[o][5] = shop.id
                        k = 0
                        for category in categories:
                            categories_array[o][k] = category
                            k=k+1
                        o = o +1
                    response_data = [('longitude', longitude, 'latitude', latitude, 'shops', shopsArray, 'categories', categories_array)]
 #   return render_to_response(
  #      'main-page.html',
  #      {'shops': shops, 'latitude': latitude, 'longitude': longitude},
  #      context_instance=RequestContext(request))

    return HttpResponse(json.dumps(response_data), content_type="application/json")

def get_categories(shop_id):
     cursor = connection.cursor()
     cursor.execute('''SELECT sc.name
                  from
                     shops_categories sc
                  where
                     sc.id in (
	                    select spc.categories_id from
	                      shops_permanentoffer_category spc
	                      where spc.permanentoffer_id in
	                      (SELECT sp.id
                        	FROM shops_permanentoffer sp WHERE
                        	sp.shop_id = %s)
                            ) or
                             sc.id in (
	                      select spc2.categories_id from
	                      shops_promotions_category spc2
                        	where spc2.promotions_id in
	                        (SELECT sp2.id
                        	FROM shops_promotions sp2 WHERE
                        	sp2.shop_id = %s)
                              )
                            group by sc.name
                ''', [shop_id, shop_id])
     categories = [item[0] for item in cursor.fetchall()]
     return categories

@csrf_exempt
def get_shop(request):
    if request.method == 'POST':
        shop_id = request.POST['shop_id']
        shop_details = models.Shop.objects.get(id=shop_id)
        try:
            hours_open = models.Hours.objects.get(shop_id = shop_id)
        except models.Hours.DoesNotExist:
            hours_open = None

        if hours_open is None:
            hours_open = models.Hours();

        json_response = {
            "name" : shop_details.name,
            "description" : shop_details.description,
            "phone" : shop_details.phone,
            "page" : shop_details.page,
            "email" : shop_details.email,
            "city" : shop_details.city,
            "flat": shop_details.flat,
            "street" : shop_details.street,
            "street_number" : shop_details.street_number,
       # }
       # if hours_open is not None:
       #     json_response.update(
        #        {
                 "mon_start" : hours_open.mon_start,
                 "mon_end" : hours_open.mon_end,
                "mon_act" : hours_open.mon_act,
                 "tue_start" : hours_open.tue_start,
                 "tue_end" : hours_open.tue_end,
                  "tue_act" : hours_open.tue_act,
                  "wed_start" : hours_open.wed_start,
                  "wed_end" : hours_open.wed_end,
                  "wed_act" : hours_open.wed_act,
                   "thur_start" : hours_open.thur_start,
                    "thur_end" : hours_open.thur_end,
                    "thur_act" : hours_open.thur_act,
                    "fri_start" : hours_open.frid_start,
                    "fri_end" : hours_open.frid_end,
                  "fri_act" : hours_open.frid_act,
                  "sat_start" : hours_open.sat_start,
                "sat_end" : hours_open.sat_end,
                  "sat_act" : hours_open.sat_act,
                   "sun_start" : hours_open.sun_start,
                 "sun_end" : hours_open.sun_end,
                  "sun_act" : hours_open.sun_act
        }
       #     )

        return HttpResponse(json.dumps(json_response, cls=DjangoJSONEncoder), mimetype='application/json')


def get_shop_preview(request):
    return render_to_response('shop_preview.html')


@login_required
@user_passes_test(company_check)
def add_shop(request):
     if request.method == 'POST':
        postdata = request.POST.copy()
        form = forms.ShopForm(postdata)
        if form.is_valid():

            shop = models.Shop(name=request.POST['name'], street=request.POST['street'], street_number=request.POST['street_number'], flat=request.POST['flat'],
                                              city=request.POST['city'], email=request.POST['email'],
                                              page=request.POST['page'], phone=request.POST['telephone'], description=request.POST['description'])

            if int(request.POST['shop_id']) >= 1:
                shop.id = request.POST['shop_id']

            shop.save()

            hours_model = models.Hours(mon_start=request.POST['mon_start'], mon_end=request.POST['mon_end'],
                                              tue_start=request.POST['tue_start'], tue_end=request.POST['tue_end'],
                                              wed_start=request.POST['wed_start'], wed_end=request.POST['wed_end'],
                                              thur_start=request.POST['thur_start'], thur_end=request.POST['thur_end'],
                                              frid_start=request.POST['frid_start'], frid_end=request.POST['frid_end'],
                                              sat_start=request.POST['sat_start'], sat_end=request.POST['sat_end'],
                                              sun_start=request.POST['sun_start'], sun_end=request.POST['sun_end'], shop = shop)

            hours_model_prev = models.Hours.objects.filter(shop = shop)
            if hours_model_prev.exists():
                hours_model.pk = shop.id

            hours_model.save()
            shop_company = CompanyProfile.objects.get(id=request.session['company_id'])
            shop.company_id.add(shop_company)

            if int(request.POST['shop_id']) >= 1:
                return HttpResponse(json.dumps({'message':'Zmiany zostaly zapisane!'}), mimetype='application/json')
            else:
                return HttpResponse(json.dumps({'message': 'Sklep zostal dodany!'}), mimetype='application/json')
        else:
            json_response = {
                'message': 'Sklep nie zostal dodany!'
            }
            return HttpResponse(json.dumps(json_response), status=200, mimetype='application/json')
        return HttpResponse(json.dumps(json_response), status=200, mimetype='application/json')


@login_required
@user_passes_test(company_check)
def add_promotion(request):
     if request.method == 'POST':
        postdata = request.POST.copy()
        files = request.FILES
        form = forms.PromotionOffer(postdata, files)
        if form.is_valid():
            shop = models.Shop.objects.get(id=request.POST['shop_id'])
            types = request.POST.getlist('types')
            #types.append(request.POST['category'])
            category = models.Categories.objects.filter(name__in=types)
            main_category = models.MainCategories.objects.get(name=request.POST['category'])
            PromotionModel = models.Promotions(date_from=request.POST['date_from'], date_to=request.POST['date_to'],
                                               price_before=request.POST['price-before'], price_after=request.POST['price-after'],
                                               shop=shop, name=request.POST['promotion_name'], main_category=main_category)



            if request.FILES['picture'] is not None:
                PromotionModel.picture = request.FILES['picture']
            if int(request.POST['promotion_id']) >= 1:
                PromotionModel.id = request.POST['promotion_id']
            PromotionModel.save()

            for oneCategory in category:
                PromotionModel.category.add(oneCategory.id)

            return HttpResponse(json.dumps({'success': 'true', 'id': PromotionModel.id}), status=203, mimetype='application/json')
        else:
            return HttpResponse(json.dumps({'success': 'false'}), status=400, mimetype='application/json')
        return HttpResponse(json.dumps({'success': 'false'}), status=400, mimetype='application/json')


@login_required
@user_passes_test(company_check)
def add_permanent_offer(request):
     if request.method == 'POST':
        postdata = request.POST.copy()
        files = request.FILES
        form = forms.PermanentOffer(postdata, files)
        if form.is_valid():
            shop = models.Shop.objects.get(id=request.POST['shop_id'])
            types = request.POST.getlist('types')
            #types.append(request.POST['category'])
            category = models.Categories.objects.filter(name__in=types)
            main_category = models.MainCategories.objects.get(name=request.POST['category'])

            PermanentModel = models.PermanentOffer(picture=request.FILES['picture'],
                                               price=request.POST['price'],
                                               shop=shop, name=request.POST['permanent_name'], main_category=main_category)

            if request.FILES['picture'] is not None:
                PermanentModel.picture = request.FILES['picture']
            if int(request.POST['permanent_id']) >= 1:
                PermanentModel.id = request.POST['permanent_id']
            PermanentModel.save()

            for oneCategory in category:
                PermanentModel.category.add(oneCategory.id)
            return HttpResponse(json.dumps({'success': 'true', 'id': PermanentModel.id}), status=203, mimetype='application/json')
        else:
            return HttpResponse(json.dumps({'success': 'false'}), status=400, mimetype='application/json')
        return HttpResponse(json.dumps({'success': 'false'}), status=400, mimetype='application/json')


@login_required
@user_passes_test(company_check)
def for_company(request):
    hours_open = forms.HoursOpen()
    permanent_offer = forms.PermanentOffer()
    form = forms.ShopForm()
    coupon_offer = forms.CouponOffer()
    promotion_offer = forms.PromotionOffer()
    shops = models.Shop.objects.filter(company_id=request.session['company_id'])
    template_name = 'for_companies.html'
    return render_to_response(template_name, {'shops': shops, 'promotion_offer': promotion_offer,'form': form, 'permanent_offer': permanent_offer, 'coupon_offer': coupon_offer, 'hours_open': hours_open}, context_instance=RequestContext(request))


@csrf_exempt
def get_permanent_offers(request):
    if request.method == 'POST':
        shop_id = request.POST['shop_id']
        permanent_offers = models.PermanentOffer.objects.filter(shop_id=shop_id)
        o = 0;
        k = 0;
        permanentArray = [["" for i in range(5)] for j in range(len(permanent_offers))]
        categories_array = [["" for j in range(26)] for i in range(len(permanent_offers)+1)]

        for onePermanent in permanent_offers:
            permanentArray[o][0] = onePermanent.id
            permanentArray[o][1] = onePermanent.name
            permanentArray[o][2] = onePermanent.price
            permanentArray[o][3] = onePermanent.picture.name
            permanentArray[o][4] = onePermanent.main_category.name
            categories = Categories.objects.filter(permanentoffer__id=onePermanent.id)
            for category in categories:
                categories_array[o][k] = category.name
                k=k+1
            o=o+1;
        response_data = [(permanentArray, categories_array)]
    return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), mimetype='application/json')


@login_required
@user_passes_test(company_check)
def get_permanent_offer(request):
    if request.method == 'POST':
        permanent_id = request.POST['permanent_id']
        permanent_offer = models.PermanentOffer.objects.get(id=permanent_id)
        permanentArray = [["" for i in range(5)] for j in range(1)]
        permanentArray[0][0] = permanent_offer.id
        permanentArray[0][1] = permanent_offer.name
        permanentArray[0][2] = permanent_offer.price
        permanentArray[0][3] = permanent_offer.picture.name
        permanentArray[0][4] = permanent_offer.category_id

    return HttpResponse(json.dumps({'success': permanentArray}), status=400, mimetype='application/json')


@csrf_exempt
def get_promotion_offers(request):
    if request.method == 'POST':
        shop_id = request.POST['shop_id']
        promotion_offers = models.Promotions.objects.filter(shop_id=shop_id)
        o = 0;
        k = 0;
        promotionarray = [["" for i in range(8)] for j in range(len(promotion_offers))]
        categories_array = [["" for j in range(26)] for i in range(len(promotion_offers)+1)]

        for onePromotion in promotion_offers:
            #main_category = MainCategories.objects.get(id=onePromotion.main_category)
            promotionarray[o][0] = onePromotion.id
            promotionarray[o][1] = onePromotion.name
            promotionarray[o][2] = onePromotion.picture.name
            promotionarray[o][3] = onePromotion.main_category.name
            promotionarray[o][4] = onePromotion.date_from
            promotionarray[o][5] = onePromotion.date_to
            promotionarray[o][6] = onePromotion.price_before
            promotionarray[o][7] = onePromotion.price_after
            categories = Categories.objects.filter(promotions__id=onePromotion.id)
            for category in categories:
                categories_array[o][k] = category.name
                k=k+1
            o=o+1;
        response_data = [('promotions', promotionarray, 'categories', categories_array)]
    return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), mimetype='application/json')


@login_required
@user_passes_test(company_check)
def get_promotion_offer(request):
    if request.method == 'POST':
        promotion_id = request.POST['promotion_id']
        promotion_offer = models.Promotions.objects.get(id=promotion_id)
        json_response = {
            "id" : promotion_offer.id,
            "name" :promotion_offer.name,
            "price_before" : promotion_offer.price_before,
            "price_after" : promotion_offer.price_after,
            "date_from" : promotion_offer.date_from,
            "date_to" : promotion_offer.date_to,
            "picture" : promotion_offer.picture.name,
            "category" : promotion_offer.category_id
        }
    return HttpResponse(json.dumps(json_response, cls=DjangoJSONEncoder), mimetype='application/json')


@csrf_exempt
@login_required
@user_passes_test(company_check)
def get_shop_list(request):
    shops = models.Shop.objects.filter(company_id=request.session['company_id'])
    o = 0;
    promotionarray = [["" for i in range(2)] for j in range(len(shops))]
    for shop in shops:
        promotionarray[o][0] = shop.id
        promotionarray[o][1] = shop.name
        o=o+1;
    return HttpResponse(json.dumps({'success': promotionarray}), mimetype='application/json')