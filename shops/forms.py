from django import forms
from shops import models


class AddressForm(forms.Form):
    city = forms.CharField()
    address = forms.CharField()

class ShopForm(forms.Form):
    name = forms.CharField(max_length=100)
    description = forms.CharField(widget=forms.Textarea)
    page = forms.CharField(required=False)
    city = forms.CharField()
    street = forms.CharField(required=False)
    street_number = forms.CharField()
    flat = forms.CharField(required=False)
    postcode = forms.CharField(required=False)
    telephone = forms.CharField(required=False)
    email = forms.CharField(required=False)


#    @property
#    def save(self):
#        new_shop = models.Shop.objects.create(name=self.cleaned_data['name'], address=self.cleaned_data['street'],
#                                              city=self.cleaned_data['city'], email=self.cleaned_data['email'],
 #                                             page=self.cleaned_data['page'], phone=self.cleaned_data['telephone'],
#                                              description=self.cleaned_data['description'])

#        new_shop.save()
#        return new_shop

class HoursOpen(forms.Form):
    mon_start = forms.CharField()
    mon_end = forms.CharField()
    mon_act = forms.BooleanField
    tue_start = forms.CharField()
    tue_end = forms.CharField()
    tue_act = forms.BooleanField
    wed_start = forms.CharField()
    wed_end = forms.CharField()
    wed_act = forms.BooleanField
    thur_start = forms.CharField()
    thur_end = forms.CharField()
    thur_act = forms.BooleanField
    frid_start = forms.CharField()
    frid_end = forms.CharField()
    frid_act = forms.BooleanField
    sat_start = forms.CharField()
    sat_end = forms.CharField()
    sat_act = forms.BooleanField
    sun_start = forms.CharField()
    sun_end = forms.CharField()
    sun_act = forms.BooleanField

#    @property
#    def save(self):
#        hours_open = models.Hours.objects.create(mon_start=self.cleaned_data['mon_start'], mon_end=self.cleaned_data['mon_end'],
#                                              tue_start=self.cleaned_data['tue_start'], tue_end=self.cleaned_data['tue_end'],
#                                              wed_start=self.cleaned_data['wed_start'], wed_end=self.cleaned_data['wed_end'],
 #                                             thur_start=self.cleaned_data['thur_start'], thur_end=self.cleaned_data['thur_end'],
 #                                             frid_start=self.cleaned_data['frid_start'], frid_end=self.cleaned_data['frid_end'],
 #                                             sat_start=self.cleaned_data['sat_start'], sat_end=self.cleaned_data['sat_end'],
 #                                             sun_start=self.cleaned_data['sun_start'], sun_end=self.cleaned_data['sun_end'])
#
#        hours_open.save();
#        return hours_open

class PermanentOffer(forms.Form):
    name = forms.CharField(required=False)
    permanent_id = forms.IntegerField(required=False)
    date_from = forms.DateField(required=False)
    date_to = forms.DateField(required=False)
    price = forms.CharField(required=False)
    picture = forms.FileField(required=False)
    category = forms.CharField(required=False)
    shop_id = forms.CharField(required=False)


class CouponOffer(forms.Form):
    name = forms.CharField()
    date_from = forms.DateField()
    date_to = forms.DateField()
    price = forms.CharField()
    picture = forms.FileField()
    icon = forms.CharField()

class PromotionOffer(forms.Form):
    name = forms.CharField(required=False)
    promotion_id = forms.IntegerField(required=False)
    date_from = forms.DateField(required=False)
    date_to = forms.DateField(required=False)
    price_before = forms.CharField(required=False)
    price_after = forms.CharField(required=False)
    picture = forms.FileField(required=False)
    category = forms.CharField(required=False)
    shop_id = forms.CharField(required=False)

#   @property
#    def save(self, files):
#        new_shop = models.Shop.objects.create(name=self.cleaned_data['name'],
 #                                             price_before=self.cleaned_data['price_before'],
 #                                             price_after=self.cleaned_data['price_after'],
 #                                             picture=files['picture'])

#        new_shop.save();
#        return new_shop


class Categories(forms.Form):
    name = forms.CharField()