from urllib2 import URLError
from django.db import models
from django.contrib.gis.db import models as gis_models
from django.contrib.gis import geos
from geopy.geocoders.googlev3 import GoogleV3
from geopy.geocoders.googlev3 import GeocoderQueryError
from accounts.models import CompanyProfile


# Create your models here.

class WearTypes(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Shop(models.Model):
    name = models.CharField(max_length=200)
    street = models.CharField(max_length=100)
    street_number = models.CharField(max_length=100)
    flat = models.CharField(max_length=100)
    city = models.CharField(max_length=50)
    email = models.CharField(max_length=70)
    page = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)

    location = gis_models.PointField(u"longitude/latitude",
                             geography=True, blank=True, null=True)

    company_id = models.ManyToManyField(CompanyProfile)

    gis = gis_models.GeoManager()
    objects = models.Manager()

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        if not self.location:
            address = u'%s %s' % (self.city, self.street + " " + self.street_number)
            address = address.encode('utf-8')
            geocoder = GoogleV3()
            try:
                _, latlon = geocoder.geocode(address)
            except (URLError, GeocoderQueryError, ValueError):
                pass
            else:
                point = "POINT(%s %s)" % (latlon[1], latlon[0])
                self.location = geos.fromstr(point)
        super(Shop, self).save()

class Categories(models.Model):
    name = models.CharField(max_length=50)

class MainCategories(models.Model):
    name = models.CharField(max_length=50)

class Promotions(models.Model):
    name = models.CharField(max_length=100)
    date_from = models.DateField(null=True)
    date_to = models.DateField(null=True)
    price_before = models.CharField(max_length=10)
    price_after = models.CharField(max_length=10)
    picture = models.FileField(upload_to='images')
    category = models.ManyToManyField(Categories)
    main_category = models.OneToOneField(MainCategories)
    shop = models.ForeignKey(Shop)


class PermanentOffer(models.Model):
    name = models.CharField(max_length=100)
    date_from = models.DateField(null=True)
    date_to = models.DateField(null=True)
    price = models.CharField(max_length=10)
    #price_after = models.CharField(max_length=10)
    picture = models.FileField(upload_to='images')
    category = models.ManyToManyField(Categories)
    main_category = models.OneToOneField(MainCategories)
    shop = models.ForeignKey(Shop)

#klasa ponizej musi zostac poprawiona na model innego rodzaju!
class Hours(models.Model):
    mon_start = models.CharField(max_length=2)
    mon_end = models.CharField(max_length=2)
    mon_act = models.BooleanField(default=False)
    tue_start = models.CharField(max_length=2)
    tue_end = models.CharField(max_length=2)
    tue_act = models.BooleanField(default=False)
    wed_start = models.CharField(max_length=2)
    wed_end = models.CharField(max_length=2)
    wed_act = models.BooleanField(default=False)
    thur_start = models.CharField(max_length=2)
    thur_end = models.CharField(max_length=2)
    thur_act = models.BooleanField(default=False)
    frid_start = models.CharField(max_length=2)
    frid_end = models.CharField(max_length=2)
    frid_act = models.BooleanField(default=False)
    sat_start = models.CharField(max_length=2)
    sat_end = models.CharField(max_length=2)
    sat_act = models.BooleanField(default=False)
    sun_start = models.CharField(max_length=2)
    sun_end = models.CharField(max_length=2)
    sun_act = models.BooleanField(default=False)
    shop = models.OneToOneField(Shop, primary_key=True)