from django.contrib import admin
from shops.models import Shop

__author__ = 'uziak'

class ShopAdmin(admin.ModelAdmin):
    fields = ['name', 'street', 'city', 'location']

admin.site.register(Shop,ShopAdmin)
