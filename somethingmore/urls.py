from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from accounts import views, forms
from registration import auth_urls as registration
from registration.backends.default.views import ActivationView
from django.contrib import admin
from shops import views as shopViews
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'somethingmore.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

   #url(r'^$',TemplateView.as_view(template_name='main-page.html'),name='index'),
    url(r'^$', TemplateView.as_view(template_name='main-page.html'), name='index'),
    url(r'^localization/map/$', shopViews.home, name='shop_view'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/user/loginUser/$', views.login_user, name='auth_login_user'),
    url(r'^accounts/user/login/$', views.registerUser, name='auth_login'),
    url(r'^accounts/company/register/$', views.registerCompany, name='register_company'),
    url(r'^company/shops/list/$', shopViews.get_shop_list, name='shop_list'),
    url(r'^accounts/company/login/$', views.loginCompany, name='login_company'),
    url(r'^accounts/user/logout/$', views.logout_user, name='auth_logout'),
    url(r'^company/shop/add/$', shopViews.add_shop, name='add_shop'),
    url(r'^company/promotion/add/$', shopViews.add_promotion, name='add_promotion'),
    url(r'^accounts/company/edit/$', views.edit_company, name='add_promotion'),
    url(r'^company/permanent/add/$', shopViews.add_permanent_offer, name='add_permanent_offer'),
    url(r'^company/permanents/get/$', shopViews.get_permanent_offers, name='get_permanent_offers'),
    url(r'^company/permanent/get/$', shopViews.get_permanent_offer, name='get_permanent_offers'),
    url(r'^company/promotions/get/$', shopViews.get_promotion_offers, name='get_promotion_offers'),
    url(r'^company/promotion/get/$', shopViews.get_promotion_offer, name='get_promotion_offer'),
    url(r'^shop/preview/$', shopViews.get_shop_preview, name='shop_preview'),
    url(r'^validate/check/email/$', views.check_email, name='check_email' ),
#powyzej trzeba zmienic sposob ladowania - niech laduje sie cala, gotowa, a nie na raty (Django + jQuery)
    url(r'^accounts/activate/(?P<activation_key>\w+)/$',
                           ActivationView.as_view(),
                           name='registration_activate'),
    url(r'^activate/complete/$',
        #TemplateView.as_view(template_name='registration/activate.html'),
        views.user_activate,
        name='registration_activation_complete'),
    #url(r'^accounts/user/register/complete$', views.registerUser, name='auth_login'),
   # url(r'^accounts/user/register/$', views.registerUser, {'form_class': forms.UserRegistrationForm, 'template_name': 'registration/user-login.html'}, name='auth_login'),
    #url(r'^accounts/company/login/$', vi   ews.registerCompany, {'form_class': forms.CompanyRegistrationForm, 'template_name': 'registration/company-login.html'}, name='auth_login'),
    url(r'^accounts/company/login/$', views.registerCompany, name='company_login'),
    url(r'^for_company/', shopViews.for_company, name='company_form'),
    url(r'^company/shop/details/', shopViews.get_shop, name='get_shop'),
    url(r'^reg_complete/', TemplateView.as_view(template_name='registration/registration_complete.html'), name="reg_complete"),
    url(r'^password/change/$',registration.auth_views.password_change,
                            {'template_name': 'registration/password_change_form.html'},
                           name='auth_password_change'),
                       url(r'^password/change/done/$',
                           registration.auth_views.password_change_done,
                            {'template_name': 'registration/password_change_done.html'},
                           name='auth_password_change_done'),
                       url(r'^password/reset/$',
                           registration.auth_views.password_reset,
                            {'template_name': 'registration/password_reset_form.html'},
                           name='auth_password_reset'),
                       url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
                           registration.auth_views.password_reset_confirm,
                            {'template_name': 'registration/password_reset_confirm.html'},
                           name='auth_password_reset_confirm'),
                       url(r'^password/reset/complete/$',
                           registration.auth_views.password_reset_complete,
                            {'template_name': 'registration/password_reset_complete.html'},
                           name='auth_password_reset_complete'),
                       url(r'^password/reset/done/$',
                           registration.auth_views.password_reset_done,
                            {'template_name': 'registration/password_reset_done.html'},
                           name='auth_password_reset_done')


)
