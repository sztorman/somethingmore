#-*- coding: utf-8 -*-

"""

Django settings for somethingmore project.



For more information on this file, see

https://docs.djangoproject.com/en/1.6/topics/settings/



For the full list of settings and their values, see

-https://docs.djangoproject.com/en/1.6/ref/settings/

-"""
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production

# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!

SECRET_KEY = '9&7nquja=_4)0ymijt(&=r0k3r=cgq#*cd95y%&%5xwcpszej@'
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []
# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
   'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
   'accounts',
    'bootstrap_toolkit',
    'registration',
    'widget_tweaks',
    'django.contrib.humanize',
    'django.contrib.gis',
    'shops',
    'home',
    'lockdown'

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
   'django.contrib.messages.middleware.MessageMiddleware',

   'django.middleware.clickjacking.XFrameOptionsMiddleware',

   'lockdown.middleware.LockdownMiddleware'

)

ROOT_URLCONF = 'somethingmore.urls'

WSGI_APPLICATION = 'somethingmore.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases


#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',

#        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),

#    }

#}
DATABASES = {

   'default': {

    'ENGINE': 'django.contrib.gis.db.backends.postgis',
    'NAME': 'smore',
     'USER': 'postgres',
       'PASSWORD': 'postgres',
       'HOST': 'localhost',

   }
}

PROJECT_ROOT = '/home/uziak/PycharmProjects/somethingmore/'

TEMPLATE_DIRS = (
   # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".

    # Always use forward slashes, even on Windows.

    # Don't forget to use absolute paths, not relative paths.
   os.path.join(PROJECT_ROOT, 'templates'),
)
# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
LANGUAGE_CODE = 'pl-pl'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
PROJECT_DIR = os.path.dirname(__file__)
STATICFILES_DIRS = (os.path.join(PROJECT_DIR, 'static'), '/home/uziak/PycharmProjects/somethingmore/static/')
STATIC_ROOT=""
STATIC_URL = '/static/'
MEDIA_ROOT = '/home/uziak/PycharmProjects/somethingmore/static/media/'
MEDIA_URL = '/media/'

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

ACCOUNT_ACTIVATION_DAYS = 7
AUTHENTICATION_BACKENDS = (
   'registration_email.auth.EmailBackend',
)

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'michal.uziak@gmail.com'
EMAIL_HOST_PASSWORD = 'wania666'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'Cos wiecej!'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
LOCKDOWN_PASSWORDS = ('smore ', 'KOMBO15m')

