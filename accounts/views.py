import json
from django.contrib.auth.models import Group
from django.core.serializers.json import DjangoJSONEncoder
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, redirect
from django.views.decorators.csrf import csrf_exempt
from forms import UserRegistrationForm, CompanyRegistrationForm
from django.template import RequestContext
from registration_email.forms import EmailAuthenticationForm
from django.contrib.auth import authenticate, login, logout
from accounts.models import CompanyProfile
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist



__author__ = 'uziak'
def registerUser(request):
    if request.method == 'POST':
        postdata = request.POST.copy()
        form = UserRegistrationForm(postdata)
        if form.is_valid():
            user = form.save()
            user_group = Group.objects.get(name='user')
            user.groups.add(user_group)
        template = 'registration/registration_complete.html'
        data = {
                'results': "dupa",
            }
        return render_to_response(template, data, context_instance=RequestContext(request))
    else:
        form = UserRegistrationForm()
        template_name = 'registration/user-login.html'
        form_auth = EmailAuthenticationForm()
        return render_to_response(template_name, {'form': form, 'form_auth': form_auth}, context_instance=RequestContext(request))

def registerCompany(request):
     if request.method == 'POST':
        postdata = request.POST.copy()
        form = CompanyRegistrationForm(postdata)
        if form.is_valid():
            company = form.save()
            company_group = Group.objects.get(name='company')
            company.groups.add(company_group)
            template = 'registration/registration_complete.html'
            data = {
                'results': "dupa",
            }
            return render_to_response(template, data, context_instance=RequestContext(request))
        else:
            template = 'registration/registration_error.html'
            data = {
                'results': "dupa",
            }
            return render_to_response(template, data, context_instance=RequestContext(request))
     else:
        form = CompanyRegistrationForm()
        template_name = 'registration/company-login.html'
        form_auth = EmailAuthenticationForm()
        return render_to_response(template_name, {'form': form, 'form_auth': form_auth}, context_instance=RequestContext(request))

def loginCompany(request):
    username = request.POST['username']
    password = request.POST['password']
    company = authenticate(username=username, password=password)
    if company is not None:
        if company.is_active:
            login(request, company)
            comp_id = CompanyProfile.objects.get(company_id=company.id)
            request.session['company_id'] = comp_id.id
            return HttpResponse(json.dumps({'error': 'Zostales zalogowany!'}), status=400, mimetype='application/json')
        else:
            return HttpResponse(json.dumps({'error': 'Your account is not active!'}), status=400, mimetype='application/json')
    else:
        return HttpResponse(json.dumps({'error': 'Login lub haslo sa niepoprawne!'}), status=400, mimetype='application/json')


def login_user(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            json_response = {
                'message': 'Zostales zalogowany!'
            }
            return HttpResponse(json.dumps(json_response), mimetype='application/json')
        else:
            json_response = {
                'message': 'Login lub haslo sa niepoprawne!'
            }
            return HttpResponse(json.dumps(json_response), cls=DjangoJSONEncoder, status=400, mimetype='application/json')
    else:
        json_response = {
                'message': 'Login lub haslo sa niepoprawne!'
            }
        return HttpResponse(json.dumps(json_response), cls=DjangoJSONEncoder, status=400,  mimetype='application/json')


def edit_company(request):
    inst = CompanyProfile.objects.get(id=request.session['company_id'])
    user = User.objects.get(id=inst.company_id)
    if request.method == 'POST':
        postdata = request.POST.copy()
        form = CompanyRegistrationForm(postdata)
        if form.is_valid():
            company = form.save()
            company_group = Group.objects.get(name='company')
            company.groups.add(company_group)
            return HttpResponse(json.dumps({'success': 'true'}), status=400, mimetype='application/json')
        else:
            return HttpResponse(json.dumps({'success': 'false'}), status=400, mimetype='application/json')
    else:
        #form = CompanyRegistrationForm(instance=inst)
        form = CompanyRegistrationForm(initial={'id': inst.id,
                                                'shop_name':inst.shop_name,
                                                'representant_name':inst.representant_name,
                                                'shop_address':inst.shop_address,
                                                'representant_surname':inst.representant_surname,
                                                'shop_vin':inst.shop_vin,
                                                'shop_regon': inst.shop_regon,
                                                'representant_name': inst.representant_name,
                                                'representant_surname': inst.representant_surname,
                                                'shop_phone':inst.shop_phone}, auto_id=False)
        template_name = 'registration/company-login.html'
        form_auth = None
        return render_to_response(template_name, {'form': form, 'form_auth': form_auth}, context_instance=RequestContext(request))


def logout_user(request):
    logout(request)
    return HttpResponse(json.dumps({'error': 'Zostales wylogowany'}), status=400, mimetype='application/json')


def user_activate(request):
    template_name = 'registration/activate.html'
    return render_to_response(template_name, {'account': True}, context_instance=RequestContext(request))


@csrf_exempt
def check_email(request):
    response_str = "false"
    if request.is_ajax():
        e = request.POST.get("email")
        try:
            obj = User.objects.get(email=e)
        except ObjectDoesNotExist:
            response_str = "true"
    return HttpResponse(response_str)