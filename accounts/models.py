from django.contrib.auth.models import User
from django.db import models

__author__ = 'uziak'


class CompanyProfile(models.Model):

   company = models.ForeignKey(User, unique=True)
   shop_name = models.CharField("Name", max_length=50, blank=True, null=True)
   shop_address = models.CharField("Address", max_length=50, blank=True, null=True)
   shop_vin = models.IntegerField("Vin", max_length=9, blank=True, null=True)
   shop_regon = models.IntegerField("Regon", max_length=9, blank=True, null=True)
   representant_name = models.CharField("Representant_name", max_length=50, blank=True, null=True)
   representant_surname = models.CharField("Representant_surname", max_length=50, blank=True, null=True)
   shop_phone = models.CharField("Shop_phone", max_length=50, blank=True, null=True)
   mobile_phone = models.CharField("Mobile_phone", max_length=50, blank=True, null=True)
   shop_fax = models.CharField("Mobile_phone", max_length=50, blank=True, null=True)


class UserProfile(models.Model):

    user = models.ForeignKey(User, unique=True)
    name = models.CharField("Name", max_length=50, blank=True, null=True)
    surname = models.CharField("Username", max_length=50, blank=True, null=True)
    city = models.CharField("City", max_length=50, blank=True, null=True)
    street = models.CharField("Street", max_length=50, blank=True, null=True)
    birth_date = models.DateField("Birth_date", blank=True, null=True)
    sex = models.CharField("Sex", max_length=1)

