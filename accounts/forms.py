__author__ = 'uziak'

from django import forms
from registration.forms import RegistrationForm
from django.utils.translation import ugettext_lazy as _
from models import CompanyProfile, UserProfile
from registration.models import RegistrationProfile
from registration_email.forms import EmailRegistrationForm


class CompanyRegistrationForm(EmailRegistrationForm):
    shop_name = forms.CharField(widget=forms.TextInput(), required=False)
    shop_address = forms.CharField(widget=forms.TextInput(), required=False)
    shop_vin = forms.IntegerField(widget=forms.TextInput(), required=False)
    shop_regon = forms.IntegerField(widget=forms.TextInput(), required=False)
    representant_name = forms.CharField(widget=forms.TextInput(), required=False)
    representant_surname = forms.CharField(widget=forms.TextInput(), required=False)
    shop_phone = forms.CharField(widget=forms.TextInput(), required=False)
    mobile_phone = forms.CharField(widget=forms.TextInput(), required=False)
    shop_fax = forms.CharField(widget=forms.TextInput(), required=False)

    def save(self, profile_callback=None):
        new_company = RegistrationProfile.objects.create_inactive_user(username=self.cleaned_data['username'],
                                                                       password=self.cleaned_data['password1'],
                                                                       site="",
                                                                       email=self.cleaned_data['email'])

        new_profile = CompanyProfile(company=new_company)
        new_profile.save()
        return new_company

    class Meta:
        model = CompanyProfile


class UserRegistrationForm(EmailRegistrationForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'IMIE','class':'login-input'}), required=False)
    surname = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'NAZWISKO', 'class':'login-input'}), required=False)
    city = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'MIASTO','class':'login-input'}), required=False)
    street = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'ULICA','class':'login-input'}), required=False)
    birth_date = forms.DateField(widget=forms.TextInput(attrs={'placeholder': 'DATA URODZENIA','class':'login-input'}), required=False)

    def save(self, profile_callback=None):
        new_user = RegistrationProfile.objects.create_inactive_user(username=self.cleaned_data['username'],
                                                                    password=self.cleaned_data['password1'],
                                                                    site="",
                                                                    email=self.cleaned_data['email']
                                                                    )
        new_profile = UserProfile(user=new_user, name = self.cleaned_data['name'],
                                                surname = self.cleaned_data['surname'],
                                                city = self.cleaned_data['city'],
                                                street = self.cleaned_data['street'],
                                                birth_date = self.cleaned_data['birth_date'])
        new_profile.save()
        return new_user
