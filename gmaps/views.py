from django.shortcuts import render
from django import forms
from django.shortcuts import render_to_response
from gmapi import maps
from gmapi.forms.widgets import GoogleMap
# Create your views here.

class MapForm(forms.Form):
    map = forms.Field(widget=GoogleMap(attrs={'width':600, 'height':500}))


def index(request):
    gmap = maps.Map(opts = {
        'center': maps.LatLng(52.2, 21),
        'mapTypeId': maps.MapTypeId.ROADMAP,
        'zoom': 9,
        'mapTypeControlOptions': {
             'style': maps.MapTypeControlStyle.DROPDOWN_MENU
        },
    })

    marker = maps.Marker(opts = {
        'map': gmap,
        'position': maps.LatLng(52.2, 21),
        'label': 'Tu mieszka moja stara',
    })
    maps.event.addListener(marker, 'mouseover', 'myobj.markerOver')
    maps.event.addListener(marker, 'mouseout', 'myobj.markerOut')
    info = maps.InfoWindow({
        'content': 'Witaj!',
        'disableAutoPan': True
    })
    info.open(gmap, marker)

    context = {'form': MapForm(initial={'map': gmap})}
    return render_to_response('main-page.html', context)
